* tg.py => file that makes channel listening with the telegram bot and parse the incoming recommendation with str.py [includes Telegram API]

* str.py => A file that parse string expressions from the tg.py file and separate them into meaningful chunks for use in stock market transactions.

* btx.py =>A file that can be bought / sold automatically in the  BitTrex BitCoin stock market.  [includes BitTrex API]