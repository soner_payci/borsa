import telegram, logging, str as myStr, btx as myBtx
from telegram.ext import (Updater, CommandHandler, MessageHandler, Filters, RegexHandler,
                          ConversationHandler)

global entry
d = {}

bot = telegram.Bot(token='452057944:AAFFbQP5TjUNXT6ijdvpstblx2UM4Sd5sqw')
updater = Updater("452057944:AAFFbQP5TjUNXT6ijdvpstblx2UM4Sd5sqw")
dp = updater.dispatcher

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',level=logging.INFO)

def start(bot, update): #telegramdan mesaj geldiğinde buna göre alacak bu fonksiyonla yapacak.
	global entry
	if "Enter" in update.message.text:
		d = myStr.str_to_keyVal(update.message.text)
		coinName = myStr.get_coin_name(update.message.text)
		bot.send_message(chat_id=update.message.chat_id, text="Enter Signal Received")
		rate = d['Price']
		print(coinName)
		ticker = myBtx.my_bittrex.get_ticker(coinName)
		print(ticker)
		quantity = entry/ticker['result']['Last']
		print(quantity)
		print(myBtx.my_bittrex.buy_limit(market=coinName, quantity=quantity, rate=rate))

	elif "Exit" in update.message.text:
		d = myStr.str_to_keyVal(update.message.text)
		coinName = myStr.get_coin_name(update.message.text)
		bot.send_message(chat_id=update.message.chat_id, text="Exit Signal Received")
		rate = d['Price']
		quantity = myBtx.my_bittrex.get_balance(coinName)
		print(myBtx.my_bittrex.sell_limit(market=coinName, quantity=quantity, rate=rate))

def setEntry(bot, update):
	global entry
	entry = myStr.get_entry(update.message.text)


message_handler = MessageHandler((Filters.all & (~ Filters.command)),callback=start)
entry_handler = CommandHandler('entry',callback=setEntry)
dp.add_handler(message_handler)
dp.add_handler(entry_handler)

updater.start_polling()
updater.idle()
